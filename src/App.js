import './App.css'
import { CommunitySection } from './CommunitySection/CommunitySection'
import { JoinSection } from './JoinProgramSection/JoinSection';

function App() {
  return (
    <div className="App">
        <CommunitySection/>
        <JoinSection/>
    </div>
  );
}

export default App;
