import './userProfile.css';

export const UserProfile = ({person, lorem}) => {

    return(
        <div className="person-container">
            <img className="person-avatar" src={person.avatar} alt="avatar"/>
            <div className="person-summary">{lorem}</div>
            <div>
                <div className="person-name">{person.firstName} {person.lastName}</div>
                <div className="person-position">{person.position}</div>
            </div>
        </div>
    )
}

