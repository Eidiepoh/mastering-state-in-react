import Slider from 'react-slick';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import slideSettings from './sliderConfigurations';
import {UserProfile} from '../UserProfiles/UserProfile';
import  { LoremIpsum } from "react-lorem-ipsum/dist";

const SlideProfiles = ({profiles}) => {

    return(
        <div>
          <Slider {...slideSettings}>
          {!profiles? '':
            profiles.map(prof => 
            <UserProfile 
            lorem={<LoremIpsum
                avgWordsPerSentence={3}
                avgSentencesPerParagraph={2}
                />} 
                key={prof.id} 
                person={prof}/>)}
            </Slider>
        </div>
    )
}

export default SlideProfiles;