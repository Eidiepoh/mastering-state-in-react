import {useState, useEffect } from 'react';

const FetchData = (route) => {
    const [data, setData] = useState(null);
  
    const [error, setError] = useState(null)

    useEffect(() => {
        if (!route) return;
        fetch(`http://localhost:3000/${route}`)
        .then(response => response.json())
        .then(setData)
        .catch(setError);
    },[route])

   
    if (error) {
        console.log(error);
    }
    if (!data) return null;
    return data;
}
export default FetchData;

