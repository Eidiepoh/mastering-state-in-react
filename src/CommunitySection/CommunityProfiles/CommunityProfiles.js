import FetchData from '../FetchData/FetchData';
import SlideProfiles from '../Slider/SlideProfiles';

export const CommunityProfiles = ({display}) => {
    const data = FetchData('community');
    const styles = {
        maxWidth: '1000px',
        margin: '0 auto',
        display: display
    }
    return(
        <div  style={styles}>
            <div>
                <h2 className="community-heading heading-two">
                    {`We're proud of our products, and we're really excited \n 
                    when we get feedback from our users.`}
                </h2>
            </div>
         <SlideProfiles profiles={data} />
        </div>
    )
}