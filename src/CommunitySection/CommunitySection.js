import { useState } from 'react';
import { CommunityHeading } from './CommunityHeading/CommunityHeading';
import { CommunityProfiles } from './CommunityProfiles/CommunityProfiles';
import './CommunitySection.css'

export const CommunitySection = () => {
   const [toggleText, setToggleText] = useState('Hide Section')
   const [display, setDisplay] = useState('block')
   const setToggleState = () => {
       if(toggleText === 'Hide Section') {
           setToggleText('Show Section');
           setDisplay('none')
       } else {
           setToggleText('Hide Section');
           setDisplay('block');
       }
   }
    return(
        <div className="community-section">
            <button className="section-toggler hide" onClick={setToggleState}>
                {`${toggleText}`}
            </button>
           <CommunityHeading />
           <CommunityProfiles display={display}/>
        </div>
    )
}