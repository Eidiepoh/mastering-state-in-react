import './CommunityHeading.css';

export const CommunityHeading = () => {
    return(
        <>
            <div>
                <h1 className="community-heading heading-one">
                    {`Big Community of People Like You`}
                </h1>
            </div>
        </>
    )
}