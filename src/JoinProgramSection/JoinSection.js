import './JoinSection.css';
import { JoinHeading } from './JoinHeading/JoinHeading';
import SubStatus from './JoinMain/SubStatus/SubStatus';

export const JoinSection = () => {
   
    return(
        <div className="join-section">
           <JoinHeading/>
           <SubStatus/>
        </div>
    )
}