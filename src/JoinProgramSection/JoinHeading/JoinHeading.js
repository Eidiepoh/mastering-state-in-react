import './JoinHeading.css';

export const JoinHeading = () => {
    return(
        <div>
            <div>
                <h1 className="joinProgram-heading join-heading-one">
                    {`Join Our Program`}
                </h1>
            </div>
            <div>
                <h2 className="joinProgram-heading join-heading-two">
                    {`Sed do eiusmod tempor incididunt 
                    ut labore et dolore magna aliqua.`}
                </h2>
            </div>
        </div>
    )
}
