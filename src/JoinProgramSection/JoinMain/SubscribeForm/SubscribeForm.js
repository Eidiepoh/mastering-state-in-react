import './SubscribeForm.css';
import { useState } from 'react';

const SubscribeForm = ({subStatus, unSubStatus, emailInputStatus, ...props}) => {
    const [value, setValue] = useState('')
    const options = {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: 
        JSON.stringify({ email: value }),
    };

    const emailCheck = (e) => {
    e.preventDefault();
    fetch(`http://localhost:3000/subscribe`, options)
    .then(response => response.json())
    .then(rez => {
        if(rez.error) alert(rez.error)
        
        if(rez.success) {
            localStorage.setItem('email', value);
            props.statusSetter.setSubStatus('disable');
            props.statusSetter.setUnStubStatus('');
            props.statusSetter.setEmailInputStatus('disable');
        }
    })
    .catch(error => console.log(error))
    setValue('');
}

const cancelSubscription = (e) => {
    e.preventDefault();
    fetch(`http://localhost:3000/unsubscribe`, {method: 'POST'})
    .then(res => res.json())
    .then(response => console.log(response))
    .catch(error => console.log(error))

    localStorage.removeItem('email');
    props.statusSetter.setSubStatus('');
    props.statusSetter.setUnStubStatus('disable');
    props.statusSetter.setEmailInputStatus('');
}

const handleChange = (event) => {
    setValue(event.target.value);
  };

    return(  
        <form className="sub-form" onSubmit={emailCheck}>
            <input className={`email ${emailInputStatus}`} 
            type="email" 
            placeholder="Email"
            value={value}
            onChange={handleChange}
            />
            <button 
            className={`button subscribe ${subStatus}`}
            type="submit">
                SUBSCRIBE
            </button>
            <button className={`button unsubscribe ${unSubStatus}`} 
            onClick={cancelSubscription}>
                UNSUBSCRIBE
            </button>
        </form>
    )
}

export default SubscribeForm;
