import { useState, useEffect } from 'react';
import SubscribeForm from '../SubscribeForm/SubscribeForm';
const SubStatus = () => {
    const [subStatus, setSubStatus] = useState('');
    const [unSubStatus, setUnStubStatus] = useState('');
    const [emailInputStatus, setEmailInputStatus] = useState('');
    
    useEffect(() => {
        if(localStorage.getItem('email') ) {
            setSubStatus('disable');
            setUnStubStatus('');
            setEmailInputStatus('disable');
        } else {
            setSubStatus('');
            setUnStubStatus('disable');
            setEmailInputStatus('');
        }
    },[])

    return (
        <SubscribeForm 
        statusSetter={{setSubStatus, setUnStubStatus, setEmailInputStatus}} 
        subStatus={subStatus} 
        unSubStatus={unSubStatus} 
        emailInputStatus={emailInputStatus}/>
    )
}

export default SubStatus;
